/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugaspraktikumguru;

/**
 *
 * @author ROG SERIE
 */
class Guru {
  private String nama;
    private String mapel;
    
    //Konstruktor
    public Guru (String nama, String mapel)
    {
        this.nama = nama;
        this.mapel = mapel;
    }
    //Method
    public void info ()
    {
        System.out.println("Nama Guru   : "+this.nama);
        System.out.println("Mapel       : "+this.mapel);
    }  
}
