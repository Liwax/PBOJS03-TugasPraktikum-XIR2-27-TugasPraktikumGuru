/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugaspraktikumguru;

/**
 *
 * @author ROG SERIE
 */
class Hari extends Mengajar
{
    private int hari;
    private int jam;
    //Kontruktor 
    public Hari (String nama, String mapel, String kelas, int hari, int jam)
    {
        super(nama, mapel, kelas);
        this.hari = hari;
        this.jam = jam;
    }
    //Method
    public void info ()
    {
        int hari = this.hari;
        int jam = this.jam;
        
        super.info();
        System.out.println("Mengajar "+hari+" kali seminggu");
        System.out.println("Mengajar "+jam+" jam sehari");
        
    }
}