/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugaspraktikumguru;

/**
 *
 * @author ROG SERIE
 */
class Mengajar extends Guru //class turunan dari class Guru
{
    private String kelas;
    //Kontruktor 
    public Mengajar (String nama, String mapel, String kelas)
    {
        super(nama, mapel);
        this.kelas = kelas;
    }
    //Method
    public void info ()
    {
        super.info();
        System.out.println("Mengajar kelas  : " + this.kelas);
        
    }
}
